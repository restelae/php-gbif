<?php

namespace ResTelae\Gbif;

/**
 * GBIF: occurrences.
 */
class Occurrences extends Gbif {

  /**
   * Gets details for a single, interpreted occurrence.
   *
   * @param int $key
   *   A GBIF occurrence key.
   *
   * @return array
   *   An array of results.
   */
  public function get(int $key) {
    return $this->gbifGet('occurrence/' . $key);
  }

  /**
   * Gets a verbatim occurrence record without any interpretation.
   *
   * @param int $key
   *   A GBIF occurrence key.
   *
   * @return array
   *   An array of results.
   */
  public function getVerbatim(int $key) {
    return $this->gbifGet('occurrence/' . $key . '/verbatim');
  }

  /**
   * Gets a single occurrence fragment in its raw form (XML or JSON).
   *
   * @param int $key
   *   A GBIF occurrence key.
   *
   * @return array
   *   An array of results.
   */
  public function getFragment(int $key) {
    return $this->gbifGet('occurrence/' . $key . '/fragment');
  }

  /**
   * Search GBIF occurrences.
   *
   * Usage:
   * @code
   *   use ResTelae\Gbif\Occurrences;
   *
   *   $occ = new Occurrences();
   *   $species = new Species();
   *
   *   $occ->search(['taxonKey' => 3329049]);
   *
   *   // Return 2 results, this is the default by the way.
   *   $occ->search(['taxonKey' => 3329049, 'limit' => 2]);
   *
   *   // Instead of getting a taxon key first, you can search for a name
   *   // directly. However, note that using this approach (with
   *   // 'scientificName' => '...') you are getting synonyms too. The results
   *   // for using `scientifcName` and `taxonKey` parameters are the same in
   *   // this case, but I wouldn't be surprised if for some names they return
   *   // different results.
   *   $occ->search(['scientificName' => 'Ursus americanus']);
   *   $sp = $species->nameBackbone([
   *     'name' => 'Ursus americanus',
   *     'rank' => 'species',
   *   ]);
   *   $occ->search(['taxonKey' = $sp['usageKey']]);
   *
   *   // Search by dataset key.
   *   $occ->search([
   *     'datasetKey' => '7b5d6a48-f762-11e1-a439-00145eb45e9a',
   *     'limit' =>20,
   *   ]);
   *
   *   // Search by catalog number.
   *   $occ->search(['catalogNumber' => '49366',  limit => 20]);
   *   $occ->search([
   *     'catalogNumber' => ['49366', 'Bird.27847588'],
   *     'limit' => 20,
   *   ]);
   *
   *   // Use paging parameters (limit and offset) to page. Note the different
   *   // results for the two queries below.
   *   $occ->search([
   *     'datasetKey' => '7b5d6a48-f762-11e1-a439-00145eb45e9a',
   *     'offset' => 10,
   *     'limit' => 5,
   *   ]);
   *   $occ->search([
   *     'datasetKey' => '7b5d6a48-f762-11e1-a439-00145eb45e9a',
   *     'offset' => 20,
   *     'limit' => 5,
   *   ]);
   *
   *   // Many dataset keys.
   *   $occ->search([
   *     'datasetKey' => [
   *       '50c9509d-22c7-4a22-a47d-8c48425ef4a7',
   *       '7b5d6a48-f762-11e1-a439-00145eb45e9a',
   *     ],
   *     'limit' => 20,
   *   ]);
   *
   *   // Search by collector name.
   *   $res = $occ->search(['recordedBy' => 'smith', 'limit' => 20]);
   *
   *   // Many collector names.
   *   $occ->search(['recordedBy' => ['smith', 'BJ Stacey'], 'limit' => 20]);
   *
   *   // recordedByID.
   *   $occ->search([
   *     'recordedByID' => 'https://orcid.org/0000-0003-1691-239X',
   *     'limit' => 3,
   *   ]);
   *
   *   // identifiedByID.
   *   $occ->search([
   *     'identifiedByID' => 'https://orcid.org/0000-0003-1691-239X',
   *     'limit' => 3,
   *   ]);
   *
   *   // Search for many species.
   *   $splist = ['Cyanocitta stelleri', 'Junco hyemalis', 'Aix sponsa'];
   *   $keys = [];
   *   foreach ($splist as $sp) {
   *     $keys[] = $species->nameSuggest($sp)[0]['key'];
   *   }
   *   $out = [];
   *   foreach ($keys as $key) {
   *     $out[] = $occ->search(['taxonKey' => $key, 'limit' => 1]);
   *   }
   *
   *   // Search - q parameter.
   *   $occ->search(['q' => 'kingfisher', 'limit' => 20]);
   *   // Spell check - only works with the `search` parameter spelled
   *   // correctly - same result as above call.
   *   $occ->search(['q' => 'kingfisher', 'limit' => 20, 'spellCheck' = TRUE]);
   *   // Spelled incorrectly - stops with suggested spelling.
   *   $occ->search(['q' => 'kajsdkla', 'limit' => 20, 'spellCheck' => TRUE]);
   *   // Spelled incorrectly - stops with many suggested spellings and number
   *   // of results for each.
   *   $occ->search(['q' => 'helir', 'limit' => 20, 'spellCheck' => TRUE]);
   *
   *   // Search on latitude and longitude.
   *   $occ->search([
   *     'decimalLatitude' => 50,
   *     'decimalLongitude' => 10,
   *     'limit' => 2,
   *   ]);
   *
   *   // Search on a bounding box in well known text format.
   *   $occ->search([
   *     'geometry' => 'POLYGON((30.1 10.1, 10 20, 20 40, 40 40, 30.1 10.1))',
   *     'limit' => 20,
   *   ]);
   *   $key = $species->nameSuggest(['q' => 'Aesculus hippocastanum'])[0]['key'];
   *   $occ->search([
   *     'taxonKey' => $key,
   *     'geometry' => 'POLYGON((30.1 10.1, 10 20, 20 40, 40 40, 30.1 10.1))',
   *     'limit' => 20,
   *   ]);
   *   // Multipolygon.
   *   $wkt = 'MULTIPOLYGON(((-123 38, -123 43, -116 43, -116 38, -123 38)),((-97 41, -97 45, -93 45, -93 41, -97 41)))';
   *   $occ->search(['geometry' => $wkt, 'limit' => 20]);
   *
   *   // Search on country.
   *   $occ->search('country' => 'US', 'limit' => 20);
   *   $occ->search('country' => 'FR', 'limit' => 20);
   *   $occ->search('country' => 'DE', 'limit' => 20);
   *
   *   // Get only occurrences with lat/long data.
   *   $occ->search([
   *     'taxonKey' => $key,
   *     'hasCoordinate' => TRUE,
   *     'limit' => 20,
   *     ]);
   *
   *   // Get only occurrences that were recorded as living specimens.
   *   $occ->search([
   *     'taxonKey' => $key,
   *     'basisOfRecord' => 'LIVING_SPECIMEN',
   *     'hasCoordinate' => TRUE,
   *     'limit' => 20,
   *   ]);
   *
   *   // Get occurrences for a particular eventDate.
   *   $occ->search(['taxonKey' => $key, 'eventDate' => '2013', 'limit' => 20]);
   *   $occ->search(['taxonKey' => $key, 'year' => '2013', limit => 20]);
   *   $occ->search(['taxonKey' => $key, 'month' => '6', 'limit' => 20]);
   *
   *   // Get occurrences based on depth.
   *   $key = $species->nameBackbone(['name' => 'Salmo salar', 'kingdom' => 'animals'])['usageKey'];
   *   $occ->search(['taxonKey' => $key, 'depth' => '5', 'limit' => 20]);
   *
   *   // Get occurrences based on elevation.
   *   $key = $species->nameBackbone(['name' => 'Puma concolor', 'kingdom' => 'animals'])['usageKey'];
   *   $occ->search([
   *     'taxonKey' => $key,
   *     'elevation' => 50,
   *     'hasCoordinate' => TRUE,
   *     'limit' => 20,
   *   ]);
   *
   *   // Get occurrences based on institutionCode.
   *   $occ->search(['institutionCode' => 'TLMF', 'limit' =>20]);
   *
   *   // Get occurrences based on collectionCode.
   *   $occ->search([
   *     'collectionCode' => 'Floristic Databases MV - Higher Plants',
   *     'limit' => 20,
   *   ]);
   *
   *   // Get only those occurrences with spatial issues.
   *   $occ->search(['
   *     taxonKey' => $key,
   *     'hasGeospatialIssue' => TRUE,
   *     'limit' => 20,
   *   ]);
   *
   *   // Search using a query string.
   *   $occ->search(['q' => 'kingfisher', 'limit' => 20]);
   *
   *   // Range queries. See Detail for parameters that support range queries.
   *   // This is a range depth, with lower/upper limits in character string.
   *   $occ->search(['depth' => '50,100']);
   *
   *   // Range search with year.
   *   $occ->search(['year' => '1999,2000', limit => 20]);
   *
   *   // Range search with latitude.
   *   $occ->search(['decimalLatitude' => '29.59,29.6']);
   *
   *   // Search by specimen type status. Look for possible values of the
   *   // typeStatus parameter looking at the typestatus dataset.
   *   $occ->search(['typeStatus' => 'allotype']);
   *
   *   // Search by specimen record number. This is the record number of the
   *   // person/group that submitted the data, not GBIF's numbers. You can see
   *   // that many different groups have record number 1, so not super helpful.
   *   $occ->search(['recordNumber' => 1]);
   *
   *   // Search by last time interpreted: Date the record was last modified in
   *   // GBIF. The lastInterpreted parameter accepts ISO 8601 format dates,
   *   // including yyyy, yyyy-MM, yyyy-MM-dd, or MM-dd. Range queries are
   *   // accepted for lastInterpreted.
   *   $occ->search(['lastInterpreted' => '2014-04-01']);
   *
   *   // Search by continent. One of africa, antarctica, asia, europe,
   *   // north_america, oceania, or south_america.
   *   $occ->search(['continent' => 'south_america']);
   *   $occ->search(['continent' => 'africa']);
   *   $occ->search(['continent' => 'oceania']);
   *   $occ->search(['continent' => 'antarctica']);
   *
   *   // Search for occurrences with images.
   *   $occ->search(['mediatype' => 'StillImage']);
   *   $occ->search(['mediatype' => 'MovingImage']);
   *   $x = $occ->search(['mediatype' => 'Sound']);
   *
   *   // Query based on issues.
   *   $occ->search(['taxonKey' => 1, 'issue' => 'DEPTH_UNLIKELY');
   *   $occ->search([
   *     'taxonKey' => 1,
   *     'issue' => ['DEPTH_UNLIKELY','COORDINATE_ROUNDED'],
   *   ]);
   *   // Show all records in the Arizona State Lichen Collection that can't be
   *   matched to the GBIF backbone properly:
   *   $occ->search([
   *     'datasetKey' => '84c0e1a0-f762-11e1-a439-00145eb45e9a',
   *     issue => ['TAXON_MATCH_NONE','TAXON_MATCH_HIGHERRANK'],
   *   ]);
   *
   *   // If you pass in an invalid polygon you get hopefully informative
   *   // errors. The WKT string is fine, but GBIF says bad polygon.
   *   $wkt = 'POLYGON((-178.59375 64.83258989321493,-165.9375 59.24622380205539,-147.3046875 59.065977905449806,-130.78125 51.04484764446178,-125.859375 36.70806354647625,-112.1484375 23.367471303759686,-105.1171875 16.093320185359257,-86.8359375 9.23767076398516,-82.96875 2.9485268155066175,-82.6171875 -14.812060061226388,-74.8828125 -18.849111862023985,-77.34375 -47.661687803329166,-84.375 -49.975955187343295,174.7265625 -50.649460483096114,179.296875 -42.19189902447192,-176.8359375 -35.634976650677295,176.8359375 -31.835565983656227,163.4765625 -6.528187613695323,152.578125 1.894796132058301,135.703125 4.702353722559447,127.96875 15.077427674847987,127.96875 23.689804541429606,139.921875 32.06861069132688,149.4140625 42.65416193033991,159.2578125 48.3160811030533,168.3984375 57.019804336633165,178.2421875 59.95776046458139,-179.6484375 61.16708631440347,-178.59375 64.83258989321493))';
   *   $occ->search(['geometry' => $wkt]);
   *
   *   // Faceting. Return no occurrence records with limit=0;
   *   $x = $occ->search(['facet' => 'country', 'limit' => 0]);
   *   $x['facets'];
   *
   *   // Also return occurrence records.
   *   $x = $occ->search(['facet' => 'establishmentMeans', 'limit' => 10]);
   *   $x['facets'];
   *   $x['results'];
   *
   *   // Multiple facet variables.
   *   $x = $occ->search([
   *     'facet' => ['country', 'basisOfRecord'],
   *     'limit' => 10,
   *   ]);
   *   $x['results'];
   *   $x['facets'];
   *   $x['facets']['country'];
   *   $x['facets']['basisOfRecord'];
   *   $x['facets']['basisOfRecord']['count'];
   *
   *   // Set a minimum facet count.
   *   $x = $occ->search([
   *     'facet' => 'country',
   *     'facetMincount' => 30000000,
   *     'limit' => 0,
   *   ]);
   *   $x['facets'];
   *
   *   // Paging per each faceted variable. Do so by passing in variables like
   *   // 'country' + '_facetLimit' = 'country_facetLimit' or 'country' +
   *   '_facetOffset' = 'country_facetOffset'.
   *   $x = $occ->search([
   *     'facet' => ['country', 'basisOfRecord', 'hasCoordinate'],
   *     'country_facetLimit' => 3,
   *     'basisOfRecord_facetLimit' => 6,
   *     'limit' => 0,
   *   ]);
   *   $x['facets'];
   * @endcode
   *
   * @param array $args
   *   An associative array, with the following elements:
   *   - 'taxonKey' (integer): A GBIF occurrence identifier.
   *   - 'q' (string): Simple search parameter. The value for this parameter can
   *     be a simple word or a phrase.
   *   - 'spellCheck' (boolean): If ``TRUE`` ask GBIF to check your spelling of
   *     the value passed to the ``search`` parameter. IMPORTANT: This only
   *     checks the input to the ``search`` parameter, and no others. Default:
   *     ``FALSE``.
   *   - 'repatriated' (string): Searches for records whose publishing country
   *     is different from the country where the record was recorded in.
   *   - 'kingdomKey' (integer): Kingdom classification key.
   *   - 'phylumKey' (integer): Phylum classification key.
   *   - 'classKey' (integer): Class classification key
   *   - 'orderKey' (integer): Order classification key
   *   - 'familyKey' (integer): Family classification key
   *   - 'genusKey' (integer): Genus classification key
   *   - 'subgenusKey' (integer): Subgenus classification key
   *   - 'scientificName' (string): A scientific name from the GBIF backbone.
   *     All included and synonym taxa are included in the search.
   *   - 'datasetKey' (string): The occurrence dataset key (a uuid).
   *   - 'catalogNumber' (string): An identifier of any form assigned by the
   *     source within a physical collection or digital dataset for the record
   *     which may not be unique, but should be fairly unique in combination
   *     with the institution and collection code.
   *   - 'recordedBy' (string): The person who recorded the occurrence.
   *   - 'recordedByID' (string): Identifier (e.g. ORCID) for the person who
   *     recorded the occurrence.
   *   - 'identifiedByID' (string): Identifier (e.g. ORCID) for the person who
   *     provided the taxonomic identification of the occurrence.
   *   - 'collectionCode' (string): An identifier of any form assigned by the
   *     source to identify the physical collection or digital dataset uniquely
   *     within the text of an institution.
   *   - 'institutionCode' (string): An identifier of any form assigned by the
   *     source to identify the institution the record belongs to. Not
   *     guaranteed to be unique.
   *   - 'country' (string): The 2-letter country code (as per ISO-3166-1) of
   *     the country in which the occurrence was recorded. See here
   *     http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.
   *   - 'basisOfRecord' (string): Basis of record, as defined in our
   *     BasisOfRecord enum here
   *     https://gbif.github.io/gbif-api/apidocs/org/gbif/api/vocabulary/BasisOfRecord.html
   *     Acceptable values are':
   *     - ``FOSSIL_SPECIMEN`` An occurrence record describing a fossilized
   *       specimen.
   *     - ``HUMAN_OBSERVATION`` An occurrence record describing an observation
   *       made by one or more people.
   *     - ``LITERATURE`` An occurrence record based on literature alone.
   *     - ``LIVING_SPECIMEN`` An occurrence record describing a living
   *       specimen.
   *     - ``MACHINE_OBSERVATION`` An occurrence record describing an
   *       observation made by a machine.
   *     - ``OBSERVATION`` An occurrence record describing an observation.
   *     - ``PRESERVED_SPECIMEN`` An occurrence record describing a preserved
   *       specimen.
   *     - ``UNKNOWN`` Unknown basis for the record.
   *   - 'eventDate' (string): Occurrence date in ISO 8601 format: yyyy,
   *     yyyy-MM, yyyy-MM-dd, or MM-dd. Supports range queries, smaller,larger
   *     (e.g., ``1990,1991``, whereas ``1991,1990`` wouldn't work).
   *   - 'year' (integer): The 4 digit year. A year of 98 will be interpreted as
   *     AD 98. Supports range queries, smaller,larger (e.g., ``1990,1991``,
   *     whereas ``1991,1990`` wouldn't work).
   *   - 'month' (integer): The month of the year, starting with 1 for January.
   *     Supports range queries, smaller,larger (e.g., ``1,2``, whereas ``2,1``
   *     wouldn't work).
   *   - 'decimalLatitude' (float): Latitude in decimals between -90 and 90
   *     based on WGS 84. Supports range queries, smaller,larger (e.g.,
   *     ``25,30``, whereas ``30,25`` wouldn't work).
   *   - 'decimalLongitude' (float): Longitude in decimals between -180 and 180
   *     based on WGS 84. Supports range queries (e.g., ``-0.4,-0.2``, whereas
   *     ``-0.2,-0.4`` wouldn't work).
   *   - 'publishingCountry' (string): The 2-letter country code (as per
   *     ISO-3166-1) of the country in which the occurrence was recorded.
   *   - 'elevation' (integer): Elevation in meters above sea level. Supports
   *     range queries, smaller,larger (e.g., ``5,30``, whereas ``30,5``
   *     wouldn't work).
   *   - 'depth' (integer): Depth in meters relative to elevation. For example
   *     10 meters below a lake surface with given elevation. Supports range
   *     queries, smaller,larger (e.g., ``5,30``, whereas ``30,5`` wouldn't
   *     work).
   *   - 'geometry' (string): Searches for occurrences inside a polygon
   *     described in Well Known Text (WKT) format. A WKT shape written as
   *     either POINT, LINESTRING, LINEARRING POLYGON, or MULTIPOLYGON. Example
   *     of a polygon: ``((30.1 10.1, 20, 20 40, 40 40, 30.1 10.1))`` would be
   *     queried as http://bit.ly/1BzNwDq.
   *     Polygons must have counter-clockwise ordering of points.
   *   - 'hasGeospatialIssue' (boolean): Includes/excludes occurrence records
   *     which contain spatial issues (as determined in our record
   *     interpretation), i.e. ``hasGeospatialIssue=TRUE`` returns only those
   *     records with spatial issues while ``hasGeospatialIssue=FALSE`` includes
   *     only records without spatial issues. The absence of this parameter
   *     returns any record with or without spatial issues.
   *   - 'issue' (string): One or more of many possible issues with each
   *     occurrence record. See Details. Issues passed to this parameter filter
   *     results by the issue.
   *   - 'hasCoordinate' (boolean): Return only occurence records with lat/long
   *     data (``TRUE``) or all records (``FALSE``, default).
   *   - 'typeStatus' (string): Type status of the specimen. One of many
   *     options. See ?typestatus.
   *   - 'recordNumber' (integer): Number recorded by collector of the data,
   *     different from GBIF record number. See
   *     http://rs.tdwg.org/dwc/terms/#recordNumber for more info.
   *   - 'lastInterpreted (string): Date the record was last modified in GBIF,
   *     in ISO 8601 format': yyyy, yyyy-MM, yyyy-MM-dd, or MM-dd. Supports
   *     range queries, smaller,larger (e.g., ``1990,1991``, whereas
   *     ``1991,1990`` wouldn't work).
   *   - 'continent' (string): Continent. One of ``africa``, ``antarctica``,
   *     ``asia``, ``europe``, ``north_america`` (North America includes the
   *     Caribbean and reaches down and includes Panama), ``oceania``, or
   *     ``south_america``.
   *   - 'fields' (string): Default (``all``) returns all fields. ``minimal``
   *     returns just taxon name, key, latitude, and longitude. Or specify each
   *     field you want returned by name, e.g.
   *     ``fields = c('name','latitude','elevation')``.
   *   - 'mediatype' (string): Media type. Default is ``NULL``, so no filtering
   *     on mediatype. Options': ``NULL``, ``MovingImage``, ``Sound``, and
   *     ``StillImage``
   *   - 'limit' (integer): Number of results to return. Default': ``300``.
   *   - 'offset' (integer): Record to start at. Default': ``0``.
   *   - 'facet' (string): a character vector of length 1 or greater.
   *   - 'establishmentMeans (string): [str] EstablishmentMeans, possible values
   *     include': INTRODUCED, INVASIVE, MANAGED, NATIVE, NATURALISED,
   *     UNCERTAIN.
   *   - 'facetMincount' (integer): minimum number of records to be included in
   *     the faceting results.
   *   - 'facetMultiselect' (boolean): Set to ``TRUE`` to still return counts
   *     for values that are not currently filtered. See examples. Default:
   *     ``False``.
   *
   * @return array
   *   An array of results.
   */
  public function search(array $args) {
    // Default values.
    $args += [
      'limit' => 300,
      'offset' => 0,
    ];

    // Representation of booleans.
    if (isset($args['hasCoordinate'])) {
      $args['hasCoordinate'] = $this->bool2str($args['hasCoordinate']);
    }
    if (isset($args['hasGeospatialIssue'])) {
      $args['hasGeospatialIssue'] = $this->bool2str($args['hasGeospatialIssue']);
    }
    if (isset($args['spellCheck'])) {
      $args['spellCheck'] = $this->bool2str($args['spellCheck']);
    }
    if (isset($args['facetMultiSelect'])) {
      $args['facetMultiSelect'] = $this->bool2str($args['facetMultiSelect']);
    }

    return $this->gbifGet('occurrence/search', $args);
  }

}
