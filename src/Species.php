<?php

namespace ResTelae\Gbif;

/**
 * GBIF: species.
 */
class Species extends Gbif {

  /**
   * Lookup details for specific names in all taxonomies in GBIF.
   *
   * Usage:
   * @code
   *   use ResTelae\Gbif\Species;
   *
   *   $species = new Species();
   *
   *   // All data for species #1;
   *   $species->nameUsage([], 'all', 1]);
   *
   *   // Name usage for a taxonomic name.
   *   $species->nameUsage(['name' => 'Puma', rank' => 'GENUS'])
   *
   *   // All name usages.
   *   $species->nameUsage();
   *
   *   // References for a name usage.
   *   $species->nameUsage([], 'references', 2435099);
   *
   *   // Species profiles, descriptions.
   *   $species->nameUsage([], 'speciesProfiles', 3119195);
   *   $species->nameUsage([], 'descriptions', 3119195);
   *   $species->nameUsage([], 'children', 2435099);
   *
   *   // Vernacular names for a name usage.
   *   $species->nameUsage([], 'vernacularNames', 3119195);
   *
   *   // Limit number of results returned.
   *   $species->nameUsage(['limit' => 3], 'vernacularNames', 3119195);
   *
   *   // Search for names by dataset with datasetKey parameter.
   *   $species->nameUsage('[
   *     datasetKey' => 'd7dddbf4-2cf0-4f39-9b2a-bb099caae36c',
   *   ]);
   *
   *   // Search for a particular language.
   *   $species->nameUsage(['language' => 'FRENCH'], 'vernacularNames', 3119195);
   * @endcode
   *
   * @param array $args
   *   An associative array, with the following elements:
   *   - 'name' (string): filters by a case insensitive, canonical namestring,
   *     e.g. 'Puma concolor'.
   *   - 'language' (string): language, default is English.
   *   - 'datasetKey' (string): filters by the dataset's key (a uuid).
   *   - 'sourceId' (integer): filters by the source identifier.
   *   - 'rank' (string): taxonomic rank. Filters by taxonomic rank as one of:
   *     'CLASS', 'CULTIVAR', 'CULTIVAR_GROUP', 'DOMAIN', 'FAMILY', 'FORM',
   *     'GENUS', 'INFORMAL', 'INFRAGENERIC_NAME', 'INFRAORDER',
   *     'INFRASPECIFIC_NAME', 'INFRASUBSPECIFIC_NAME', 'KINGDOM', 'ORDER',
   *     'PHYLUM', 'SECTION', 'SERIES', 'SPECIES', 'STRAIN', 'SUBCLASS',
   *     'SUBFAMILY', 'SUBFORM', 'SUBGENUS', 'SUBKINGDOM', 'SUBORDER',
   *     'SUBPHYLUM', 'SUBSECTION', 'SUBSERIES', 'SUBSPECIES', 'SUBTRIBE',
   *     'SUBVARIETY', 'SUPERCLASS', 'SUPERFAMILY', 'SUPERORDER', 'SUPERPHYLUM',
   *     'SUPRAGENERIC_NAME', 'TRIBE', 'UNRANKED', 'VARIETY'.
   *   - 'limit' (integer): number of records to return. Default: 100. Maximum:
   *     1000 (optional).
   *   - 'offset' (integer): record number to start at (optional).
   * @param string $data
   *   The type of data to get. Default: 'all'. Options: 'all', 'verbatim',
   *   'name', 'parents', 'children', 'related', 'synonyms', 'descriptions',
   *   'distributions', 'media', 'references', 'speciesProfiles',
   *   'vernacularNames', 'typeSpecimens', 'root'.
   * @param int $key
   *   A GBIF key for a taxon.
   * @param string $uuid
   *   A uuid for a dataset. Should give exact same results as datasetKey. Only
   *   relevant when $data is 'root'.
   * @param string $short_name
   *   A short name. Only relevant when data is 'root', in which case either
   *   $uuid or $short_name must be set. Taken into account if $uuid is NULL.
   *
   * @return array
   *   An array of results.
   *
   * @see http://www.gbif.org/developer/species#nameUsages
   *
   * @throws \ResTelae\Gbif\GbifException
   *   When some arguments are wrong.
   */
  public function nameUsage(array $args = [], $data = 'all', $key = NULL, $uuid = NULL, $short_name = NULL) {
    // Default values.
    $args += [
      'limit' => 100,
    ];

    $data_choices = [
      'all',
      'verbatim',
      'name',
      'parents',
      'children',
      'related',
      'synonyms',
      'descriptions',
      'distributions',
      'media',
      'references',
      'speciesProfiles',
      'vernacularNames',
      'typeSpecimens',
      'root',
    ];

    if (!in_array($data, $data_choices)) {
      throw new GbifException('Illegal choice for `data`');
    }

    if ($data != 'all' && !$key) {
      throw new GbifException('You must specify a key if `data` does not equal `all`');
    }

    if ($data == 'all') {
      $uri = $key ? 'species/' . $key : 'species';
    }
    elseif ($data == 'root') {
      if (!$uuid && !$short_name) {
        throw new GbifException('`uuid` and `short_name` cannot be both NULL if `data` equals "root"');
      }
      $uri = $uuid ? 'species/root/' . $uuid : 'species/root/' . $short_name;
    }
    else {
      $uri = 'species/' . $key . '/' . $data;
    }

    return $this->gbifGet($uri, $args);
  }

  /**
   * Lookup for a specific taxon name.
   *
   * Usage:
   * @code
   *   use ResTelae\Gbif\Species;
   *
   *   $species = new Species();
   *
   *   // All data for species #2435099;
   *   $species->nameUsageByKey(2435099);
   * @endcode
   *
   * @param int $key
   *   A GBIF key for a taxon.
   *
   * @return array
   *   An array of results.
   */
  public function nameUsageByKey($key) {
    return $this->nameUsage([], 'all', $key);
  }

  /**
   * Lookup names in the GBIF backbone taxonomy.
   *
   * If you are looking for behavior similar to the GBIF website when you search
   * for a name, `name_backbone` may be what you want. For example, a search for
   * *Lantanophaga pusillidactyla* on the GBIF website and with `name_backbone`
   * will give back as a first result the correct name *Lantanophaga
   * pusillidactylus*.
   *
   * A list for a single taxon with many slots (with ``verbose=FALSE`` -
   * default), or a list of length two, first element for the suggested taxon
   * match, and a data.frame with alternative name suggestions resulting from
   * fuzzy matching (with ``verbose=TRUE``).
   *
   * If you don't get a match GBIF gives back a list of length 3 with slots
   * synonym, confidence, and ``matchType='NONE'``.
   *
   * Usage:
   * @code
   *   use ResTelae\Gbif\Species;
   *
   *   $species = new Species();
   *
   *   $species->nameBackbone([
   *     'name' => 'Helianthus annuus',
   *     'kingdom' => 'plants',
   *   ]);
   *   $species->nameBackbone([
   *     'name' => 'Helianthus',
   *     'rank' => 'genus',
   *     'kingdom' => 'plants',
   *   ]);
   *   $species->nameBackbone([
   *     'name' => 'Poa',
   *     'rank' => 'genus',
   *     'family' => 'Poaceae',
   *   ]);
   *
   *   // Verbose - gives back alternatives.
   *   $species->nameBackbone([
   *     'name' => 'Helianthus annuus',
   *     'kingdom' => 'plants',
   *     'verbose' => TRUE,
   *   ]);
   *   // Strictness.
   *   $species->nameBackbone([
   *     'name' => 'Poa',
   *     'kingdom' => 'plants',
   *     'verbose' => TRUE,
   *     'strict' => FALSE,
   *   ]);
   *   $species->nameBackbone([
   *     'name' => 'Helianthus annuus',
   *     'kingdom' => 'plants',
   *     'verbose' => TRUE,
   *     'strict' => TRUE,
   *   ]);
   *
   *   // Non-existent name.
   *   $species->nameBackbone(['name' => 'Aso']);
   *
   *   // Multiple equal matches.
   *   $species->nameBackbone(['name' => 'Oeanante']);
   * @endcode
   *
   * @param array $args
   *   An associative array, with the following elements:
   *   - 'name' (string): full scientific name potentially with authorship
   *     (required).
   *   - 'rank' (string): the rank given as our rank enum (optional).
   *   - 'kingdom' (string): if provided default matching will also try to match
   *     against this if no direct match is found for the name alone (optional).
   *   - 'phylum' (string): if provided default matching will also try to match
   *     against this if no direct match is found for the name alone (optional).
   *   - 'class' (string): if provided default matching will also try to match
   *     against this if no direct match is found for the name alone (optional).
   *   - 'order' (string): if provided default matching will also try to match
   *     against this if no direct match is found for the name alone (optional).
   *   - 'family' (string): if provided default matching will also try to match
   *     against this if no direct match is found for the name alone (optional).
   *   - 'genus' (string): if provided default matching will also try to match
   *     against this if no direct match is found for the name alone (optional).
   *   - 'strict' (boolean): if ``TRUE`` it (fuzzy) matches only the given name,
   *     but never a taxon in the upper classification (optional).
   *   - 'verbose' (boolean): if ``TRUE`` show alternative matches considered
   *     which had been rejected.
   *   - 'offset' (integer): record to start at. Default: ``0``.
   *   - 'limit' (integer): number of results to return. Default: ``100``.
   *
   * @return array
   *   An array of results.
   *
   * @see https://www.gbif.org/developer/species#searching
   */
  public function nameBackbone(array $args) {
    // Default values.
    $args += [
      'limit' => 100,
    ];

    if (isset($args['strict'])) {
      $args['strict'] = $this->bool2str($args['strict']);
    }
    if (isset($args['verbose'])) {
      $args['verbose'] = $this->bool2str($args['verbose']);
    }

    return $this->gbifGet('species/match', $args);
  }

  /**
   * Lookup names in all taxonomies in GBIF.
   *
   * This service uses fuzzy lookup so that you can put in partial names and you
   * should get back those things that match. See examples below.
   *
   * Usage:
   * @code
   *   use ResTelae\Gbif\Species;
   *
   *   $species = new Species();
   *
   *   // Look up names like mammalia.
   *   $species->nameLookup(['q' => 'mammalia']);
   *
   *   // Paging.
   *   $species->nameLookup(['q' => 'mammalia', 'limit' => 1]);
   *   $species->nameLookup(['q' => 'mammalia', 'limit' => 1, 'offset' => 2]);
   *
   *   // Large requests, use offset parameter.
   *   $first = $species->nameLookup([
   *     'q' => 'mammalia',
   *     'limit' => 1000,
   *   ]);
   *   $second = $species->nameLookup(['
   *     q' => 'mammalia',
   *     'limit' => 1000,
   *     offset => 1000,
   *   ]);
   *
   *   // Get all data and parse it, removing descriptions field which can be
   *   // quite long.
   *   $out = $species->nameLookup([
   *     'q' => 'Helianthus annuus',
   *     'rank' => 'species',
   *   ]);
   *   $res = $out['results'];
   *   foreach ($res as &$value) {
   *     unset($value['descriptions']);
   *   }
   *
   *   // Fuzzy searching.
   *   $species->nameLookup(['q' => 'Heli', 'rank' => 'genus'])
   *
   *   // Limit records to certain number.
   *   $species->nameLookup([
   *     'q' => 'Helianthus annuus',
   *     'rank' => 'species',
   *     'limit' => 2,
   *   ]);
   *
   *   // Query by habitat.
   *   $species->nameLookup(['habitat' => 'terrestrial', 'limit' => 2]);
   *   $species->nameLookup(['habitat' => 'marine', 'limit' => 2]);
   *   $species->nameLookup(['habitat' => 'freshwater', 'limit' => 2]);
   *
   *   // Using faceting.
   *   $species->nameLookup([
   *     'facet' => 'status',
   *     'limit' => 0,
   *     'facetMincount' => 70000,
   *   ]);
   *   $species->nameLookup([
   *     'facet' => ['status', 'higherTaxonKey'],
   *     'limit' => 0,
   *     'facetMincount' => 70000,
   *   ]);
   *
   *   $species->nameLookup(['facet' => 'nameType', 'limit' => 0]);
   *   $species->nameLookup(['facet' => 'habitat', 'limit' => 0]);
   *   $species->nameLookup(['facet' => 'datasetKey', 'limit' => 0]);
   *   $species->nameLookup(['facet' => 'rank', 'limit' => 0]);
   *   $species->nameLookup(['facet' => 'isExtinct', 'limit' => 0]);
   *
   *   // Text highlighting.
   *   $species->nameLookup(['q' => 'plant', 'hl' => TRUE, 'limit' => 30]);
   *
   *   // Lookup by datasetKey.
   *   $species->nameLookup([
   *     'datasetKey' => '3f8a1297-3259-4700-91fc-acc4170b27ce',
   *   ]);
   * @endcode
   *
   * @param array $args
   *   An associative array, with the following elements:
   *   - 'q' (string): query term(s) for full text search (optional).
   *   - 'rank' (string): `CLASS', 'CULTIVAR', 'CULTIVAR_GROUP', 'DOMAIN',
   *     'FAMILY', 'FORM', 'GENUS', 'INFORMAL', 'INFRAGENERIC_NAME',
   *     'INFRAORDER', 'INFRASPECIFIC_NAME', 'INFRASUBSPECIFIC_NAME', 'KINGDOM',
   *     'ORDER', 'PHYLUM', 'SECTION', 'SERIES', 'SPECIES', 'STRAIN',
   *     'SUBCLASS', 'SUBFAMILY', 'SUBFORM', 'SUBGENUS', 'SUBKINGDOM',
   *     'SUBORDER', 'SUBPHYLUM', 'SUBSECTION', 'SUBSERIES', 'SUBSPECIES',
   *     'SUBTRIBE', 'SUBVARIETY', 'SUPERCLASS', 'SUPERFAMILY', 'SUPERORDER',
   *     'SUPERPHYLUM', 'SUPRAGENERIC_NAME', 'TRIBE', 'UNRANKED', 'VARIETY'
   *     (optional).
   *   - 'verbose' (boolean): if ``TRUE`` show alternative matches considered
   *     which had been rejected.
   *   - 'higherTaxonomy' (string): filters by any of the higher Linnean rank
   *     keys. Note this is within the respective checklist and not searching
   *     nub keys across all checklists (optional).
   *   - 'status' (string): optional. Filters by the taxonomic status as one of:
   *     - 'ACCEPTED'.
   *     - 'DETERMINATION_SYNONYM': used for unknown child taxa referred to via
   *       spec, ssp, etc.
   *     - 'DOUBTFUL': treated as accepted, but doubtful whether this is
   *       correct.
   *     - 'HETEROTYPIC_SYNONYM': more specific subclass of 'SYNONYM'.
   *     - 'HOMOTYPIC_SYNONYM': more specific subclass of 'SYNONYM'.
   *     - 'INTERMEDIATE_RANK_SYNONYM': used in nub only.
   *     - 'MISAPPLIED': more specific subclass of 'SYNONYM'.
   *     - 'PROPARTE_SYNONYM': more specific subclass of 'SYNONYM'.
   *     - 'SYNONYM': a general synonym, the exact type is unknown.
   *   - 'isExtinct' (boolean): filters by extinction status.
   *     E.g. 'isExtinct' => TRUE.
   *   - 'habitat' (string): filters by habitat. One of 'marine', 'freshwater'
   *     or 'terrestrial' (optional).
   *   - 'nameType' (string): optional. Filters by the name type as one of:
   *     - 'BLACKLISTED': surely not a scientific name.
   *     - 'CANDIDATUS': Candidatus is a component of the taxonomic name for a
   *       bacterium that cannot be maintained in a bacteriology culture
   *       collection.
   *     - 'CULTIVAR': a cultivated plant name.
   *     - 'DOUBTFUL': doubtful whether this is a scientific name at all.
   *     - 'HYBRID': a hybrid formula (not a hybrid name).
   *     - 'INFORMAL': a scientific name with some informal addition like 'cf.'
   *       or indetermined like Abies spec.
   *     - 'SCINAME': a scientific name which is not well formed.
   *     - 'VIRUS': a virus name.
   *     - 'WELLFORMED': a well-formed scientific name according to present
   *       nomenclatural rules.
   *   - 'datasetKey' (string): filters by the dataset's key (a uuid)
   *     (optional).
   *   - 'nomenclaturalStatus' (string): not yet implemented, but will
   *     eventually allow for filtering by a nomenclatural status enum.
   *   - 'limit' (integer): number of records to return. Maximum: 1000.
   *     (optional).
   *   - 'offset' (integer): record number to start at (optional).
   *   - 'facet' (string): a list of facet names used to retrieve the 100 most
   *     frequent values for a field. Allowed facets are: 'datasetKey',
   *     'higherTaxonKey', 'rank', 'status', 'isExtinct', 'habitat', and
   *     'nameType'. Additionally 'threat' and 'nomenclaturalStatus' are legal
   *     values but not yet implemented, so data will not yet be returned for
   *     them (optional).
   *   - 'facetMincount' (integer): used in combination with the facet
   *     parameter. Set 'facetMincount={#}' to exclude facets with a count less
   *     than {#}, e.g. http://bit.ly/1bMdByP only shows the type value
   *     'ACCEPTED' because the other statuses have counts less than 7,000,000
   *     (optional).
   *   - 'facetMultiselect' (boolean): used in combination with the facet
   *     parameter. Set 'facetMultiselect' => TRUE to still return counts for
   *     values that are not currently filtered, e.g. http://bit.ly/19YLXPO
   *     still shows all status values even though status is being filtered by
   *     'status' => 'ACCEPTED' (optional).
   *   - 'type' (string): type of name. One of 'occurrence', 'checklist', or
   *     'metadata' (optional).
   *   - 'hl' (boolean): Set 'hl' => TRUE to highlight terms matching the query
   *     when in fulltext search fields. The highlight will be an emphasis tag
   *     of class 'gbifH1' e.g. 'q' => 'plant', 'hl' => TRUE'. Fulltext search
   *     fields include: 'title', 'keyword', 'country', 'publishing country',
   *     'publishing organization title', 'hosting organization title', and
   *     'description'. One additional full text field is searched which
   *     includes information from metadata documents, but the text of this
   *     field is not returned in the response (optional).
   *
   * @return array
   *   An array of results.
   *
   * @see http://www.gbif.org/developer/species#searching
   */
  public function nameLookup(array $args) {
    // Default values.
    $args += [
      'limit' => 100,
    ];

    if (isset($args['isExtinct'])) {
      $args['isExtinct'] = $this->bool2str($args['isExtinct']);
    }
    if (isset($args['facetMultiSelect'])) {
      $args['facetMultiSelect'] = $this->bool2str($args['facetMultiSelect']);
    }
    if (isset($args['hl'])) {
      $args['hl'] = $this->bool2str($args['hl']);
    }
    if (isset($args['verbose'])) {
      $args['verbose'] = $this->bool2str($args['verbose']);
    }

    return $this->gbifGet('species/search', $args);
  }

  /**
   * Autocomplete service.
   *
   * A quick and simple autocomplete service that returns up to 20 name usages
   * by doing prefix matching against the scientific name. Results are ordered
   * by relevance.
   *
   * Usage:
   * @code
   *   use ResTelae\Gbif\Species;
   *
   *   $species = new Species();
   *
   *   $species->nameSuggest(['q' => 'Puma concolor']);
   *   $x = $species->nameSuggest(['q' => 'Puma']);
   *   $species->nameSuggest(['q' => 'Puma'], 'rank' => 'genus');
   *   $species->nameSuggest(['q' => 'Puma'], 'rank' => 'subspecies');
   *   $species->nameSuggest(['q' => 'Puma'], 'rank' => 'species');
   *   $species->nameSuggest(['q' => 'Puma'], 'rank' => 'infraspecific_name');
   *   $species->nameSuggest(['q' => 'Puma'], 'limit' => 2);
   * @endcode
   *
   * @param array $args
   *   An associative array, with the following elements:
   *   - 'q' (string): simple search parameter. The value for this parameter can
   *     be a simple word or a phrase. Wildcards can be added to the simple word
   *     parameters only, e.g. 'q' => '*puma*' (required).
   *   - 'datasetKey' (string): filters by the checklist dataset key (a uuid,
   *     see examples).
   *   - 'rank' (string): a taxonomic rank. One of 'class', 'cultivar',
   *     'cultivar_group', 'domain', 'family', 'form', 'genus', 'informal',
   *     'infrageneric_name', 'infraorder', 'infraspecific_name',
   *     'infrasubspecific_name', 'kingdom', 'order', 'phylum', 'section',
   *     'series', 'species', 'strain', 'subclass', 'subfamily', 'subform',
   *     'subgenus', 'subkingdom', 'suborder', 'subphylum', 'subsection',
   *     'subseries', 'subspecies', 'subtribe', 'subvariety', 'superclass',
   *     'superfamily', 'superorder', 'superphylum', 'suprageneric_name',
   *     'tribe', 'unranked', or 'variety'.
   *   - 'limit' (integer): number of records to return. Maximum: 1000
   *     (optional).
   *   - 'offset' (integer): record number to start at (optional).
   *
   * @return array
   *   An array of results.
   *
   * @see http://www.gbif.org/developer/species#searching
   */
  public function nameSuggest(array $args) {
    // Default values.
    $args += [
      'limit' => 100,
    ];

    return $this->gbifGet('species/suggest', $args);
  }

}
