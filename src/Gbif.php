<?php

namespace ResTelae\Gbif;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * GBIF: generic class.
 */
abstract class Gbif {

  /**
   * GBIF API base URL.
   */
  const GBIF_BASEURL = 'https://api.gbif.org/v1/';

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $gbifClient;

  /**
   * Constructs a GBIF object.
   */
  public function __construct() {
    $this->gbifClient = new Client([
      'timeout' => 30,
      'base_uri' => self::GBIF_BASEURL,
    ]);
  }

  /**
   * Sends an HTTP GET request to GBIF.
   *
   * @param string $uri
   *   URI without base URI and leading slash.
   * @param array $args
   *   Arguments to be passed through the query string.
   *
   * @return array
   *   An array of results.
   *
   * @throws \ResTelae\Gbif\GbifException
   *   In case the HTTP request ends with an error.
   */
  protected function gbifGet($uri, array $args = []) {
    $query_string = $this->formatQueryString($args);
    try {
      $response = $this->gbifClient->get($uri, ['query' => $query_string]);
    }
    catch (ClientException $e) {
      throw new GbifException($e->getResponse()->getBody()->getContents(), $e->getCode(), $e);
    }

    $data = (string) $response->getBody();
    return json_decode($data, TRUE);
  }

  /**
   * Formats a query string.
   *
   * Guzzle cannot deal with duplicate URI parameters by default, so we directly
   * pass a string instead of an array.
   *
   * @param array $args
   *   Arguments to be passed through the query string.
   *
   * @return string
   *   Formatted query string.
   */
  protected function formatQueryString(array $args) {
    $query = [];

    foreach ($args as $key => $arg) {
      if (is_array($arg)) {
        foreach ($arg as $sub_arg) {
          $query[] = $key . '=' . $sub_arg;
        }
      }
      else {
        $query[] = $key . '=' . $arg;
      }
    }

    return implode('&', $query);
  }

  /**
   * Represents boolean value as literal.
   *
   * @param bool $val
   *   Boolean value to convert.
   *
   * @return string
   *   Literal representation ('true' or 'false').
   */
  protected function bool2str(bool $val) {
    return var_export($val, TRUE);
  }

}
