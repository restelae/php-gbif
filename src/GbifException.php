<?php

namespace ResTelae\Gbif;

/**
 * GBIF: exception handler.
 */
class GbifException extends \Exception {
}
