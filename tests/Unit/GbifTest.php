<?php

declare(strict_types=1);

namespace ResTelae\Gbif\Tests\Unit;

use PHPUnit\Framework\TestCase;
use ResTelae\Gbif\Gbif;
use ResTelae\Gbif\GbifException;

/**
 * @covers \ResTelae\Gbif\Gbif
 * @internal
 */
final class GbifTest extends TestCase
{
    public function providerName(): iterable
    {

        yield [
            'enumeration/license',
            [],
            [
                'http://creativecommons.org/publicdomain/zero/1.0/legalcode',
                'http://creativecommons.org/licenses/by/4.0/legalcode',
                'http://creativecommons.org/licenses/by-nc/4.0/legalcode',
                'UNSPECIFIED',
                'UNSUPPORTED',
            ],
        ];
    }

    /** @dataProvider providerName */
    public function testClientGet(string $uri, array $args, array $expected): void
    {
        static::assertSame($expected, $this->getGbif()->gbifGet($uri, $args));
    }

    public function testClientGetShouldThrowExceptionOnClientException(): void
    {
        $this->expectException(GbifException::class);

        $this->getGbif()->gbifGet('invalid');
    }

    public function providerQueryString(): iterable
    {
        yield 'Empty args' => [[], ''];
        yield 'Flat args' => [
            ['q' => 'Puma', 'rank' => 'GENUS', 'offset' => 0, 'limit' => 20],
            'q=Puma&rank=GENUS&offset=0&limit=20',
        ];
        yield 'Nested args' => [
            ['q' => ['Puma', 'Cougar'], 'rank' => 'GENUS', 'offset' => 0, 'limit' => 20],
            'q=Puma&q=Cougar&rank=GENUS&offset=0&limit=20',
        ];
    }

    /** @dataProvider providerQueryString */
    public function testFormatQueryString(array $args, string $expected): void
    {
        static::assertSame($expected, $this->getGbif()->formatQueryString($args));
    }

    public function providerBooleans(): iterable
    {
        yield 'TRUE' => [true, 'true'];
        yield 'FALSE' => [false, 'false'];
    }

    /** @dataProvider providerBooleans */
    public function testBooleanToStringConversion(bool $val, string $expected): void
    {
        static::assertSame($expected, $this->getGbif()->bool2str($val));
    }

    private function getGbif(): Gbif
    {
        return new class() extends Gbif {
            public function gbifGet($uri, array $args = [])
            {
                return parent::gbifGet($uri, $args);
            }

            public function formatQueryString(array $args)
            {
                return parent::formatQueryString($args);
            }

            public function bool2str(bool $val)
            {
                return parent::bool2str($val);
            }
        };
    }
}
