<?php declare(strict_types=1);

namespace ResTelae\Gbif\Tests\Unit;

use PHPUnit\Framework\TestCase;
use ResTelae\Gbif\Occurrences;

/**
 * @covers \ResTelae\Gbif\Occurrences
 * @internal
 */
final class OccurrencesTest extends TestCase
{
    /** @var Occurrences */
    private $occurrences;

    protected function setUp(): void
    {
        parent::setUp();

        $this->occurrences = new Occurrences();
    }

    public function testShouldGetValidResponseFromGet(): void
    {
        $response = $this->occurrences->get(1258202889);

        $this->assertEquals("Alfaroa Standl.", $response['scientificName']);
    }

    public function testShouldGetValidResponseFromGetVerbatim(): void
    {
        $response = $this->occurrences->getVerbatim(1258202889);

        $this->assertEquals("Alfaroa Standl.", $response['http://rs.tdwg.org/dwc/terms/scientificName']);
    }

    public function testShouldGetValidResponseFromGetFragment(): void
    {
        $response = $this->occurrences->getFragment(1258202889);

        $this->assertEquals("Alfaroa Standl.", $response['scientificName']);
    }
}
