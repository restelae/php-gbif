<?php declare(strict_types=1);

namespace ResTelae\Gbif\Tests\Unit;

use PHPUnit\Framework\TestCase;
use ResTelae\Gbif\GbifException;
use ResTelae\Gbif\Species;

/**
 * @covers \ResTelae\Gbif\Species
 * @internal
 */
final class SpeciesTest extends TestCase
{
    /** @var Species */
    private $species;

    protected function setUp(): void
    {
        parent::setUp();

        $this->species = new Species();
    }

    public function testShouldThrowExceptionIfInvalidDataChoice(): void
    {
        $this->expectException(GbifException::class);
        $this->expectExceptionMessage('Illegal choice for `data`');
        $this->species->nameUsage([], 'invalid');
    }

    public function testShouldThrowExceptionIfNoDataKeyProvided(): void
    {
        $this->expectException(GbifException::class);
        $this->expectExceptionMessage('You must specify a key if `data` does not equal `all`');
        $this->species->nameUsage([], 'name');
    }

    public function testShouldThrowExceptionOnRootDataIfNoUuidOrShortNameProvided(): void
    {
        $this->expectException(GbifException::class);
        $this->expectExceptionMessage('`uuid` and `short_name` cannot be both NULL if `data` equals "root"');
        $this->species->nameUsage([], 'root', '123');
    }

    public function providerNameUsageList(): iterable
    {
        yield 'Empty args' => [[], 'all', null, null, null, ['offset' => 0, 'limit' => 100, 'endOfRecords' => false]];
        yield 'Root with UUID args' => [[], 'root', 63306619, 'd7dddbf4-2cf0-4f39-9b2a-bb099caae36c', null, ['offset' => 0, 'limit' => 100, 'endOfRecords' => true]];
    }

    /** @dataProvider providerNameUsageList */
    public function testShouldGetValidListResponseWithValidArguments(array $args, ?string $data, ?int $key, ?string $uuid, ?string $short_name, array $expected): void
    {
        $response = $this->species->nameUsage($args, $data, $key, $uuid, $short_name);

        $this->assertEquals($expected['offset'], $response['offset']);
        $this->assertEquals($expected['limit'], $response['limit']);
        $this->assertSame($expected['endOfRecords'], $response['endOfRecords']);
        $this->assertIsArray($response['results']);
    }

    public function providerNameUsageRecord(): iterable
    {
        yield '' => [[], 'name', 2435098, null, null, ['canonicalName' => 'Puma']];
    }

    /** @dataProvider providerNameUsageRecord */
    public function testShouldGetValidRecordResponseWithValidArguments(array $args, ?string $data, ?int $key, ?string $uuid, ?string $short_name, array $expected): void
    {
        $response = $this->species->nameUsage($args, $data, $key, $uuid, $short_name);

        $this->assertSame($expected['canonicalName'], $response['canonicalName']);
    }

    public function testShouldGetValidResponseFromNameUsageByKey(): void
    {
        $response = $this->species->nameUsageByKey(5231190);

        $this->assertEquals(5231190, $response['key']);
        $this->assertEquals('Animalia', $response['kingdom']);
        $this->assertEquals('Passer domesticus', $response['species']);
    }

    public function testShouldGetValidResponseFromNameBackbone(): void
    {
        $response = $this->species->nameBackbone(
            [
                'verbose' => true,
                'kingdom' => 'Plantae',
                'name'    => 'Oenante',
            ]
        );

        $this->assertEquals(3034893, $response['usageKey']);
        $this->assertEquals('Oenanthe', $response['canonicalName']);
        $this->assertEquals('GENUS', $response['rank']);
    }

    public function testShouldGetValidResponseFromNameLookup(): void
    {
        $response = $this->species->nameLookup(
            [
                'q'      => 'Puma',
                'rank'   => 'GENUS',
                'offset' => 0,
                'limit'  => 20,
            ]
        );

        $this->assertEquals(0, $response['offset']);
        $this->assertEquals(20, $response['limit']);
        $this->assertIsArray($response['results']);
        $this->assertEquals('Puma', $response['results'][0]['genus']);
        $this->assertEquals('Puma', $response['results'][0]['canonicalName']);
    }

    public function testShouldGetValidResponseFromNameSuggest(): void
    {
        $response = $this->species->nameSuggest(
            [
                'q'      => 'Pum',
                'rank'   => 'GENUS',
                'offset' => 0,
                'limit'  => 20,
            ]
        );

        $this->assertIsArray($response);
        $this->assertCount(20, $response);
        $this->assertStringStartsWith('Pum', $response[0]['genus']);
        $this->assertStringStartsWith('Pum', $response[0]['canonicalName']);
    }
}
