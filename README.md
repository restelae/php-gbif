# PHP GBIF client

First attempt of a PHP client for the [GBIF API](https://www.gbif.org/developer/summary). See its sibling projects in [R](https://github.com/ropensci/rgbif), [Python](https://github.com/sckott/pygbif) and [Ruby](https://github.com/sckott/gbifrb). Only occurrences (search and retrieval) and species are currently supported, but features will be added if this project gets enough funding.

A [Drupal module](https://www.drupal.org/project/gbif2) uses this client to enable users to list GBIF occurrences with the Views module on their websites.

## Installation

```
composer require restelae/php-gbif
```

## Usage

### Get an occurrence knowing its ID (key)

```
<?php

use ResTelae\Gbif\Occurrences;

$occ = new Occurrences();
// Returns an array of results.
$occ->get(2550051996);
```

### Search occurrences

```
<?php

use ResTelae\Gbif\Occurrences;

$occ = new Occurrences();

// Returns an array of results.
$occ->search(['taxonKey' => 3329049]);

// Search by dataset key.
$occ->search([
  'datasetKey' => '7b5d6a48-f762-11e1-a439-00145eb45e9a',
  'limit' => 20,
]);

```

[More information](https://restelae.pages.res-telae.cat/php-gbif/class_res_telae_1_1_gbif_1_1_occurrences.html) about the Occurrences class.

### Species

#### Lookup names in the GBIF backbone taxonomy.

If you are looking for behavior similar to the GBIF website when you search for a name, `name_backbone` may be what you want. For example, a search for *Lantanophaga pusillidactyla* on the GBIF website and with `name_backbone` will give back as a first result the correct name *Lantanophaga pusillidactylus*.

```
<?php

use ResTelae\Gbif\Species;

$species = new Species();

$species->nameBackbone([
  'name' => 'Helianthus annuus',
  'kingdom' => 'plants',
]);
```

#### Lookup names in all taxonomies in GBIF.

This service uses fuzzy lookup so that you can put in partial names and you should get back those things that match.

```
<?php

use ResTelae\Gbif\Species;

$species = new Species();

// Look up names like mammalia.
$species->nameLookup(['q' => 'mammalia']);

// Paging.
$species->nameLookup(['q' => 'mammalia', 'limit' => 1]);
$species->nameLookup(['q' => 'mammalia', 'limit' => 1, 'offset' => 2]);
```

#### Lookup details for specific names in all taxonomies in GBIF

Lookup details for specific names:

```
<?php

use ResTelae\Gbif\Species;

$species = new Species();

// All data for species #1;
$species->nameUsage([], 'all', 1]);

// Name usage for a taxonomic name.
$species->nameUsage(['name' => 'Puma', rank' => 'GENUS'])
```

Lookup for a specific taxon name, knowing its key:

```
<?php

use ResTelae\Gbif\Species;

$species = new Species();

// All data for species #2435099;
$species->nameUsage(2435099]);
```

#### Name suggestions (autocomplete service)

A quick and simple autocomplete service that returns up to 20 name usages by doing prefix matching against the scientific name. Results are ordered by relevance.

```
<?php

use ResTelae\Gbif\Species;

$species = new Species();

$species->nameSuggest(['q' => 'Puma'], 'rank' => 'genus');
```

[More information](https://restelae.pages.res-telae.cat/php-gbif/class_res_telae_1_1_gbif_1_1_species.html) about the Species class.
